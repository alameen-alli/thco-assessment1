"use client"

import * as React from "react";
import { ThemeProvider } from "@mui/material/styles";
import { AppRouterCacheProvider } from "@mui/material-nextjs/v13-appRouter";
import CssBaseline from "@mui/material/CssBaseline";
import theme from "@/theme";
import DashboardAppBar from "@/components/DashboardLayout/dashboard-app-bar";
import DashboardDrawer from "@/components/DashboardLayout/dashboard-drawer";
import { Box, Container } from "@mui/material";
import { usePathname } from "next/navigation";

export default function DashBoardLayout(props: { children: React.ReactNode }) {
  // Get Url Path
  const pathname = usePathname(); // Get the current pathname using usePathname


  return (
    <html lang="en">
      <body style={{ backgroundColor: "#f4f5fa" }}>
        <AppRouterCacheProvider options={{ enableCssLayer: true }}>
          <ThemeProvider theme={theme}>
            {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
            <CssBaseline />

            <Box sx={{ display: "flex"}}>
              <DashboardAppBar pathname={pathname} />
              <DashboardDrawer />
              <Box
                component="main"
                sx={{
                  height: "100vh",
                  overflow: "auto",
                  width: "100%"
                }}
              >
                {props.children}
              </Box>
            </Box>
          </ThemeProvider>
        </AppRouterCacheProvider>
      </body>
    </html>
  );
}
