"use client"

import Messenger from "@/components/Messenger";
import { interClassName } from "@/fonts/fonts";
import { Box, Button, Container, Stack, Typography } from "@mui/material";
import React from "react";

const Conversations = () => {
  return (
    <Box sx={{ mt: "116px", mb: "22px", mx: "22px" }}>
      <Stack direction="row" justifyContent="space-between" alignItems="center">
        <Typography
          className={interClassName}
          fontWeight="500"
          sx={{ fontSize: { xs: "14px", sm: "16px" } }}
        >
          Conversations with Customers
        </Typography>
        <Button
          className={interClassName}
          sx={{
            fontSize: "14px",
            fontFamily: "500",
            height: "36px",
            textTransform: "capitalize",
            backgroundColor: "#5570F1",
            width: "161px",
            borderRadius: "12px",
          }}
          variant="contained"
        >
          New Message
        </Button>
      </Stack>

      <Messenger />
    </Box>
  );
};

export default Conversations;
