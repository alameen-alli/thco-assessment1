"use client"

import { AbandonedCartCard } from "@/components/cards/AbandonedCartCard";
import { AllOrdersCard } from "@/components/cards/AllOrdersCard";
import { AllProductsCard } from "@/components/cards/AllProductsCard";
import { CustomersCard } from "@/components/cards/CustomersCard";
import { MarketingCard } from "@/components/cards/MarketingCard";
import { RecentOrdersCard } from "@/components/cards/RecentOrdersCard";
import { SalesCard } from "@/components/cards/SalesCard";
import { SummaryCard } from "@/components/cards/SummaryCard";
import { gridSpacing } from "@/store/constant";
import { faker } from "@faker-js/faker";
import { Grid } from "@mui/material";

export default function Dashboard() {
  return (
    <Grid
      mt="116px"
      mb="22px"
      mx="22px"
      spacing={2}
      // alignContent="flex-start"
    >
      <Grid item xs={12}>
        <Grid container spacing={3}>
          <Grid item xs={12} xl={3.5}>
            <SalesCard />
          </Grid>
          <Grid item xs={12} xl={3.5}>
            <CustomersCard />
          </Grid>
          <Grid item xs={12} xl={5}>
            <AllOrdersCard />
          </Grid>
        </Grid>
      </Grid>

      <Grid mt="24px" item xs={12}>
        <Grid container spacing={3}>
          <Grid item xs={12} xl={7}>
            <Grid container spacing={gridSpacing}>
              <Grid item xs={12} lg={6}>
                <MarketingCard />
              </Grid>
              <Grid item xs={12} lg={6}>
                <Grid container spacing={gridSpacing}>
                  <Grid item xs={12}>
                    <AllProductsCard />
                  </Grid>
                  <Grid item xs={12}>
                    <AbandonedCartCard />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Grid marginTop="20px" container spacing={gridSpacing}>
              <Grid item xs={12}>
                <SummaryCard />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} xl={5}>
            <RecentOrdersCard
              list={[...Array(9)].map((_, index) => ({
                id: faker.string.uuid(),
                title: "IPhone 13",
                description: "₦730,000.00 x 1",
                image: `/assets/images/covers/iphone.png`,
                postedAt: "12 Sept 2022",
                status: index % 2 === 0 ? "pending" : "completed",
              }))}
            />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
