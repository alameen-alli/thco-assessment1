import { Poppins, Inter } from 'next/font/google';

// Define the Poppins font
export const poppins = Poppins({
  weight: ['300', '400', '500', '700'],
  subsets: ['latin'],
  display: 'swap',
});

// Define the Inter font
export const inter = Inter({
  weight: ['300', '400', '500', '700'],
  subsets: ['latin'],
  display: 'swap',
});

// Export the className properties of the fonts
export const poppinsClassName = poppins.className;
export const interClassName = inter.className;
