import React from "react";
import Typography from "@mui/material/Typography";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Link from "@mui/material/Link";
import { usePathname } from "next/navigation";
import { interClassName } from "@/fonts/fonts";

function handleClick(event: React.MouseEvent<HTMLDivElement, MouseEvent>) {
  event.preventDefault();
  console.info("You clicked a breadcrumb.");
}

export default function IconBreadcrumbs() {
  const pathname = usePathname(); // Get the current pathname using usePathname

  // Get the path segments from the pathname
  const pathSegments = pathname.split("/").filter((segment) => segment !== "");

  return (
    <div role="presentation" onClick={handleClick}>
      <Breadcrumbs aria-label="breadcrumb">
        {/* Render Home breadcrumb */}
        <Link
          underline="hover"
          sx={{ display: "flex", alignItems: "center" }}
          color="inherit"
          href="/"
        >
          <img src="/assets/icons/navbar/ic_home.svg" alt="Home" />
        </Link>

        {/* Render breadcrumbs for each path segment */}
        {pathSegments.map((segment, index) => (
          <Link
            key={segment}
            underline="hover"
            sx={{
              display: "flex",
              alignItems: "center",
              textTransform: "capitalize",
            }}
            color="inherit"
            href={`/${pathSegments.slice(0, index + 1).join("/")}`} // Generate the URL for the breadcrumb
          >
            <Typography className={interClassName} variant="body2">
              {segment}
            </Typography>
          </Link>
        ))}
      </Breadcrumbs>
    </div>
  );
}
