import React from "react";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { deepPurple } from "@mui/material/colors";
import FormControl from "@mui/material/FormControl";
import { menuClasses } from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Select, { selectClasses } from "@mui/material/Select";
import { interClassName } from "@/fonts/fonts";

export function ThisWeekSelect() {
  const [val, setVal] = React.useState(1);
  return (
    <FormControl>
      <Select
      className={interClassName}
        disableUnderline
        variant="standard"
        MenuProps={{
          anchorOrigin: {
            vertical: "bottom",
            horizontal: "left",
          },
          transformOrigin: {
            vertical: "top",
            horizontal: "left",
          },
          sx: {
            marginBlock: "0.5rem",
            [`& .${menuClasses.paper}`]: {
              borderRadius: "12px",
            },
            [`& .${menuClasses.list}`]: {
              paddingTop: 0,
              paddingBottom: 0,
              background: "white",
              "& li": {
                paddingTop: "12px",
                paddingBottom: "12px",
              },
              "& li:hover": {
                background: deepPurple[50],
              },
              "& li.Mui-selected": {
                color: "white",
                background: "#5570F1",
              },
              "& li.Mui-selected:hover": {
                background: deepPurple[500],
              },
            },
          },
        }}
        IconComponent={ExpandMoreIcon}
        value={val}
        onChange={(event) => setVal(event.target.value as number)}
        sx={{
        //   background: "#FEF5EA",
        width: "100px",
          [`& .${selectClasses.select}`]: {
            background: "transparent",
            color: "#BEC0CA",
            fontSize: "12px",
            borderRadius: "12px",
            // paddingLeft: "12px",
            // paddingRight: "12px",
            paddingTop: "7.5px",
            paddingBottom: "7.5px",
            "&:focus": {
              borderRadius: "12px",
              background: "transparent",
              borderColor: deepPurple[100],
            },
          },
          [`& .${selectClasses.icon}`]: {
            // right: "12px",
            color: "#BEC0CA",
            fontWeight: "500",
            fontSize: "24px"
          },
        }}
      >
        <MenuItem value={1}>This Week</MenuItem>
      </Select>
    </FormControl>
  );
}