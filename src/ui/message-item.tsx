import { interClassName } from "@/fonts/fonts";
import { Box, Link, Stack, Typography } from "@mui/material";

export default function MessageItems() {
  return (
    <Box>
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        gap="8px"
        // spacing={2}
        sx={{
          borderBottom: "1px solid #F1F3F9",
          borderRight: "4px solid #5570F1",
          padding: "12px 24px",
          backgroundColor: "#F7F7FC",
        }}
      >
        <Stack direction="row" gap="14px">
          <Box
            component="img"
            alt="user"
            src="/assets/images/profile/profile.png"
            sx={{ width: 48, height: 48, borderRadius: 1.5, flexShrink: 0 }}
          />

          <Stack justifyContent="space-between">
            <Link
              className={interClassName}
              color="#45464E"
              variant="subtitle2"
              underline="hover"
              noWrap
              fontSize="16px"
              fontWeight="400"
            >
              Jane Doe
            </Link>

            <Typography
              className={interClassName}
              variant="body2"
              color="#8B8D97"
              fontWeight="400"
              fontSize="14px"
              sx={{ color: "#33343A" }}
              noWrap
            >
              Hi, i want make enquiries about yo...
            </Typography>
          </Stack>
        </Stack>

        <Stack justifyContent="space-between" alignItems="flex-end" gap="10px">
          <Typography
            className={interClassName}
            variant="caption"
            sx={{
              flexShrink: 0,
              color: "#1C1D22",
              backgroundColor: "#FEF5EA",
              padding: "2px 7px",
              borderRadius: "8px",
              fontSize: "12px"
            }}
          >
            New
          </Typography>
          <Typography
            className={interClassName}
            variant="caption"
            fontSize="12px"
            sx={{ flexShrink: 0, color: "#8B8D97" }}
          >
            12:55 am
          </Typography>
        </Stack>
      </Stack>

      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        gap="8px"
        // spacing={2}
        sx={{
          borderBottom: "1px solid #F1F3F9",
          padding: "12px 24px",
        }}
      >
        <Stack direction="row" gap="14px">
          <Box
            component="img"
            alt="user"
            src="/assets/images/profile/profile1.png"
            sx={{ width: 48, height: 48, borderRadius: 1.5, flexShrink: 0 }}
          />

          <Stack justifyContent="space-between">
            <Link
              className={interClassName}
              color="#45464E"
              variant="subtitle2"
              underline="hover"
              noWrap
              fontSize="16px"
              fontWeight="400"
            >
              Janet Adebayo
            </Link>

            <Typography
              className={interClassName}
              variant="body2"
              color="#8B8D97"
              fontWeight="400"
              fontSize="14px"
              sx={{ color: "#33343A" }}
              noWrap
            >
              Hi, i want make enquiries about yo...
            </Typography>
          </Stack>
        </Stack>

        <Stack justifyContent="space-between" alignItems="flex-end" gap="10px">
          <Typography
            className={interClassName}
            variant="caption"
            sx={{
              flexShrink: 0,
              color: "#1C1D22",
              backgroundColor: "#FEF5EA",
              padding: "2px 7px",
              borderRadius: "8px",
              fontSize: "12px"
            }}
          >
            New
          </Typography>
          <Typography
            className={interClassName}
            variant="caption"
            fontSize="12px"
            sx={{ flexShrink: 0, color: "#8B8D97" }}
          >
            12:55 am
          </Typography>
        </Stack>
      </Stack>

      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        gap="8px"
        sx={{
          borderBottom: "1px solid #F1F3F9",
          padding: "12px 24px",
        }}
      >
        <Stack direction="row" gap="14px">
          <Box
            component="img"
            alt="user"
            src="/assets/images/profile/profile2.png"
            sx={{ width: 48, height: 48, borderRadius: 1.5, flexShrink: 0 }}
          />

          <Stack justifyContent="space-between">
            <Link
              className={interClassName}
              color="#45464E"
              variant="subtitle2"
              underline="hover"
              noWrap
              fontSize="16px"
              fontWeight="400"
            >
              Kunle Adekunle
            </Link>

            <Typography
              className={interClassName}
              variant="body2"
              color="#8B8D97"
              fontWeight="400"
              fontSize="14px"
              sx={{ color: "#33343A" }}
              noWrap
            >
              Hi, i want make enquiries about yo...
            </Typography>
          </Stack>
        </Stack>

        <Stack justifyContent="space-between" alignItems="flex-end" gap="10px">
          <Typography
            className={interClassName}
            variant="caption"
            sx={{
              flexShrink: 0,
              color: "#1C1D22",
              backgroundColor: "#FEF5EA",
              padding: "2px 7px",
              borderRadius: "8px",
              fontSize: "12px"
            }}
          >
            New
          </Typography>
          <Typography
            className={interClassName}
            variant="caption"
            fontSize="12px"
            sx={{ flexShrink: 0, color: "#8B8D97" }}
          >
            12:55 am
          </Typography>
        </Stack>
      </Stack>

      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        gap="8px"
        // spacing={2}
        sx={{
          borderBottom: "1px solid #F1F3F9",
          padding: "12px 24px",
        }}
      >
        <Stack direction="row" gap="14px">
          <Box
            component="img"
            alt="user"
            src="/assets/images/profile/profile.png"
            sx={{ width: 48, height: 48, borderRadius: 1.5, flexShrink: 0 }}
          />

          <Stack justifyContent="space-between">
            <Link
              className={interClassName}
              color="#45464E"
              variant="subtitle2"
              underline="hover"
              noWrap
              fontSize="16px"
              fontWeight="400"
            >
              Jane Doe
            </Link>

            <Typography
              className={interClassName}
              variant="body2"
              color="#8B8D97"
              fontWeight="400"
              fontSize="14px"
              sx={{ color: "#33343A" }}
              noWrap
            >
              Hi, i want make enquiries about yo...
            </Typography>
          </Stack>
        </Stack>

        <Stack justifyContent="space-between" alignItems="flex-end" gap="10px">
          <Typography
            className={interClassName}
            variant="caption"
            sx={{
              flexShrink: 0,
              color: "#1C1D22",
              backgroundColor: "#FEF5EA",
              padding: "2px 7px",
              borderRadius: "8px",
              fontSize: "12px"
            }}
          >
            New
          </Typography>
          <Typography
            className={interClassName}
            variant="caption"
            fontSize="12px"
            sx={{ flexShrink: 0, color: "#8B8D97" }}
          >
            12:55 am
          </Typography>
        </Stack>
      </Stack>

      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        gap="8px"
        // spacing={2}
        sx={{
          borderBottom: "1px solid #F1F3F9",
          padding: "12px 24px",
        }}
      >
        <Stack direction="row" gap="14px">
          <Box
            component="img"
            alt="user"
            src="/assets/images/profile/profile1.png"
            sx={{ width: 48, height: 48, borderRadius: 1.5, flexShrink: 0 }}
          />

          <Stack justifyContent="space-between">
            <Link
              className={interClassName}
              color="#45464E"
              variant="subtitle2"
              underline="hover"
              noWrap
              fontSize="16px"
              fontWeight="400"
            >
              Janet Adebayo
            </Link>

            <Typography
              className={interClassName}
              variant="body2"
              color="#8B8D97"
              fontWeight="400"
              fontSize="14px"
              sx={{ color: "#33343A" }}
              noWrap
            >
              Hi, i want make enquiries about yo...
            </Typography>
          </Stack>
        </Stack>

        <Stack justifyContent="space-between" alignItems="flex-end" gap="10px">
          <Typography
            className={interClassName}
            variant="caption"
            sx={{
              flexShrink: 0,
              color: "#1C1D22",
              backgroundColor: "#FEF5EA",
              padding: "2px 7px",
              borderRadius: "8px",
              fontSize: "12px"
            }}
          >
            New
          </Typography>
          <Typography
            className={interClassName}
            variant="caption"
            fontSize="12px"
            sx={{ flexShrink: 0, color: "#8B8D97" }}
          >
            12:55 am
          </Typography>
        </Stack>
      </Stack>

      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        gap="8px"
        sx={{
          borderBottom: "1px solid #F1F3F9",
          padding: "12px 24px",
        }}
      >
        <Stack direction="row" gap="14px">
          <Box
            component="img"
            alt="user"
            src="/assets/images/profile/profile2.png"
            sx={{ width: 48, height: 48, borderRadius: 1.5, flexShrink: 0 }}
          />

          <Stack justifyContent="space-between">
            <Link
              className={interClassName}
              color="#45464E"
              variant="subtitle2"
              underline="hover"
              noWrap
              fontSize="16px"
              fontWeight="400"
            >
              Kunle Adekunle
            </Link>

            <Typography
              className={interClassName}
              variant="body2"
              color="#8B8D97"
              fontWeight="400"
              fontSize="14px"
              sx={{ color: "#33343A" }}
              noWrap
            >
              Hi, i want make enquiries about yo...
            </Typography>
          </Stack>
        </Stack>

        <Stack justifyContent="space-between" alignItems="flex-end" gap="10px">
          <Typography
            className={interClassName}
            variant="caption"
            sx={{
              flexShrink: 0,
              color: "#1C1D22",
              backgroundColor: "#FEF5EA",
              padding: "2px 7px",
              borderRadius: "8px",
              fontSize: "12px"
            }}
          >
            New
          </Typography>
          <Typography
            className={interClassName}
            variant="caption"
            fontSize="12px"
            sx={{ flexShrink: 0, color: "#8B8D97" }}
          >
            12:55 am
          </Typography>
        </Stack>
      </Stack>

      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        gap="8px"
        // spacing={2}
        sx={{
          borderBottom: "1px solid #F1F3F9",
          padding: "12px 24px",
        }}
      >
        <Stack direction="row" gap="14px">
          <Box
            component="img"
            alt="user"
            src="/assets/images/profile/profile.png"
            sx={{ width: 48, height: 48, borderRadius: 1.5, flexShrink: 0 }}
          />

          <Stack justifyContent="space-between">
            <Link
              className={interClassName}
              color="#45464E"
              variant="subtitle2"
              underline="hover"
              noWrap
              fontSize="16px"
              fontWeight="400"
            >
              Jane Doe
            </Link>

            <Typography
              className={interClassName}
              variant="body2"
              color="#8B8D97"
              fontWeight="400"
              fontSize="14px"
              sx={{ color: "#33343A" }}
              noWrap
            >
              Hi, i want make enquiries about yo...
            </Typography>
          </Stack>
        </Stack>

        <Stack justifyContent="space-between" alignItems="flex-end" gap="10px">
          <Typography
            className={interClassName}
            variant="caption"
            sx={{
              flexShrink: 0,
              color: "#1C1D22",
              backgroundColor: "#FEF5EA",
              padding: "2px 7px",
              borderRadius: "8px",
              fontSize: "12px"
            }}
          >
            New
          </Typography>
          <Typography
            className={interClassName}
            variant="caption"
            fontSize="12px"
            sx={{ flexShrink: 0, color: "#8B8D97" }}
          >
            12:55 am
          </Typography>
        </Stack>
      </Stack>

      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        gap="8px"
        // spacing={2}
        sx={{
          borderBottom: "1px solid #F1F3F9",
          padding: "12px 24px",
        }}
      >
        <Stack direction="row" gap="14px">
          <Box
            component="img"
            alt="user"
            src="/assets/images/profile/profile1.png"
            sx={{ width: 48, height: 48, borderRadius: 1.5, flexShrink: 0 }}
          />

          <Stack justifyContent="space-between">
            <Link
              className={interClassName}
              color="#45464E"
              variant="subtitle2"
              underline="hover"
              noWrap
              fontSize="16px"
              fontWeight="400"
            >
              Janet Adebayo
            </Link>

            <Typography
              className={interClassName}
              variant="body2"
              color="#8B8D97"
              fontWeight="400"
              fontSize="14px"
              sx={{ color: "#33343A" }}
              noWrap
            >
              Hi, i want make enquiries about yo...
            </Typography>
          </Stack>
        </Stack>

        <Stack justifyContent="space-between" alignItems="flex-end" gap="10px">
          <Typography
            className={interClassName}
            variant="caption"
            sx={{
              flexShrink: 0,
              color: "#1C1D22",
              backgroundColor: "#FEF5EA",
              padding: "2px 7px",
              borderRadius: "8px",
              fontSize: "12px"
            }}
          >
            New
          </Typography>
          <Typography
            className={interClassName}
            variant="caption"
            fontSize="12px"
            sx={{ flexShrink: 0, color: "#8B8D97" }}
          >
            12:55 am
          </Typography>
        </Stack>
      </Stack>

      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        gap="8px"
        sx={{
          borderBottom: "1px solid #F1F3F9",
          padding: "12px 24px",
        }}
      >
        <Stack direction="row" gap="14px">
          <Box
            component="img"
            alt="user"
            src="/assets/images/profile/profile2.png"
            sx={{ width: 48, height: 48, borderRadius: 1.5, flexShrink: 0 }}
          />

          <Stack justifyContent="space-between">
            <Link
              className={interClassName}
              color="#45464E"
              variant="subtitle2"
              underline="hover"
              noWrap
              fontSize="16px"
              fontWeight="400"
            >
              Kunle Adekunle
            </Link>

            <Typography
              className={interClassName}
              variant="body2"
              color="#8B8D97"
              fontWeight="400"
              fontSize="14px"
              sx={{ color: "#33343A" }}
              noWrap
            >
              Hi, i want make enquiries about yo...
            </Typography>
          </Stack>
        </Stack>

        <Stack justifyContent="space-between" alignItems="flex-end" gap="10px">
          <Typography
            className={interClassName}
            variant="caption"
            sx={{
              flexShrink: 0,
              color: "#1C1D22",
              backgroundColor: "#FEF5EA",
              padding: "2px 7px",
              borderRadius: "8px",
              fontSize: "12px"
            }}
          >
            New
          </Typography>
          <Typography
            className={interClassName}
            variant="caption"
            fontSize="12px"
            sx={{ flexShrink: 0, color: "#8B8D97" }}
          >
            12:55 am
          </Typography>
        </Stack>
      </Stack>
    </Box>
  );
}
