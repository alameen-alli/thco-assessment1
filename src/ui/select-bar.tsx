import React from "react";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { deepPurple } from "@mui/material/colors";
import FormControl from "@mui/material/FormControl";
import { menuClasses } from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Select, { selectClasses } from "@mui/material/Select";
import { interClassName } from "@/fonts/fonts";

export function SelectBar() {
  const [val, setVal] = React.useState(1);
  return (
    <FormControl
      sx={{
        maxWidth: "154px",
        width: "100%",
        display: { xs: "none", sm: "block" },
      }}
    >
      <Select
        className={interClassName}
        disableUnderline
        variant="standard"
        MenuProps={{
          anchorOrigin: {
            vertical: "bottom",
            horizontal: "left",
          },
          transformOrigin: {
            vertical: "top",
            horizontal: "left",
          },
          sx: {
            marginBlock: "0.5rem",
            [`& .${menuClasses.paper}`]: {
              borderRadius: "12px",
            },
            [`& .${menuClasses.list}`]: {
              paddingTop: 0,
              paddingBottom: 0,
              background: "white",
              "& li": {
                paddingTop: "12px",
                paddingBottom: "12px",
              },
              "& li:hover": {
                background: deepPurple[50],
              },
              "& li.Mui-selected": {
                color: "white",
                background: "#5570F1",
              },
              "& li.Mui-selected:hover": {
                background: deepPurple[500],
              },
            },
          },
        }}
        IconComponent={ExpandMoreIcon}
        value={val}
        onChange={(event) => setVal(event.target.value as number)}
        sx={{
          //   background: "#FEF5EA",
          width: "154px",
          [`& .${selectClasses.select}`]: {
            background: "#FEF5EA",
            color: "#1C1D22",
            fontSize: "14px",
            borderRadius: "12px",
            paddingLeft: "12px",
            paddingRight: "12px",
            paddingTop: "7.5px",
            paddingBottom: "7.5px",
            "&:focus": {
              borderRadius: "12px",
              background: "#FEF5EA",
              borderColor: deepPurple[100],
            },
          },
          [`& .${selectClasses.icon}`]: {
            right: "12px",
            color: "#1C1D22",
            fontWeight: "700",
            fontSize: "30px",
          },
        }}
      >
        <MenuItem value={0}>Alameen&apos;s Shop</MenuItem>
        <MenuItem value={1}>Nanny&apos;s Shop</MenuItem>
      </Select>
    </FormControl>
  );
}
