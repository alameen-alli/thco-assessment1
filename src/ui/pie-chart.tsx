import * as React from "react";
import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import UndoOutlinedIcon from "@mui/icons-material/UndoOutlined";
import { PieChart } from "@mui/x-charts";

interface DataItem {
  label: string;
  value: number;
  color: string;
}

interface ItemData extends DataItem {
  id: string;
}

const palette = ["#97A5EB", "#5570F1", "#FFCC91"];

const data1: DataItem[] = [
  { label: "", value: 400, color: "#EEF0FA" },
];

const data2: DataItem[] = [
  { label: "Purchase", value: 100, color: "#97A5EB" },
  { label: "Acquisition", value: 550, color: "#5570F1" },
  { label: "Retention", value: 350, color: "#FFCC91" },
];

const series = [
//   {
//     innerRadius: 0,
//     outerRadius: 80,
//     id: 'series-1',
//     data: data1,
//   },
  {
    innerRadius: 100,
    outerRadius: 120,
    id: "series-2",
    data: data2,
  },
];

const PieClick: React.FC = () => {
  const [itemData, setItemData] = React.useState<ItemData | null>();

  return (
    <Stack
      // direction={{ xs: "md: "row" }}
      spacing={{ xs: 0, md: 4 }}
      sx={{ width: "100%" }}
    >
      <Box>
        <PieChart
          series={series}
          width={400}
          height={300}
          slotProps={{
            legend: { hidden: true },
          }}
          // onItemClick={(event, d) => setItemData(d)}
        />
      </Box>
    </Stack>
  );
};

export default PieClick;
