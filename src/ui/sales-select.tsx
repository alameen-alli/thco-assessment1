import React from "react";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { deepPurple } from "@mui/material/colors";
import FormControl from "@mui/material/FormControl";
import { menuClasses } from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Select, { selectClasses } from "@mui/material/Select";
import { interClassName } from "@/fonts/fonts";

export function SalesSelect() {
  const [val, setVal] = React.useState(1);
  return (
    <FormControl>
      <Select
      className={interClassName}
        disableUnderline
        variant="standard"
        MenuProps={{
          anchorOrigin: {
            vertical: "bottom",
            horizontal: "left",
          },
          transformOrigin: {
            vertical: "top",
            horizontal: "left",
          },
          sx: {
            marginBlock: "0.5rem",
            [`& .${menuClasses.paper}`]: {
              borderRadius: "12px",
            },
            [`& .${menuClasses.list}`]: {
              paddingTop: 0,
              paddingBottom: 0,
              background: "white",
              "& li": {
                paddingTop: "12px",
                paddingBottom: "12px",
              },
              "& li:hover": {
                background: deepPurple[50],
              },
              "& li.Mui-selected": {
                color: "white",
                background: "#5570F1",
              },
              "& li.Mui-selected:hover": {
                background: deepPurple[500],
              },
            },
          },
        }}
        IconComponent={ExpandMoreIcon}
        value={val}
        onChange={(event) => setVal(event.target.value as number)}
        sx={{
        //   background: "#FEF5EA",
        width: "99px",
          [`& .${selectClasses.select}`]: {
            background: "#5570F114",
            color: "#5570F1",
            fontSize: "14px",
            borderRadius: "8px",
            paddingLeft: "12px",
            paddingRight: "12px",
            paddingTop: "7.5px",
            paddingBottom: "7.5px",
            "&:focus": {
              borderRadius: "8px",
              background: "#5570F114",
              borderColor: deepPurple[100],
            },
          },
          [`& .${selectClasses.icon}`]: {
            right: "14px",
            color: "#5570F1",
            fontWeight: "500",
            fontSize: "24px"
          },
        }}
      >
        <MenuItem value={1}>Sales</MenuItem>
      </Select>
    </FormControl>
  );
}