import { forwardRef, HTMLAttributes } from 'react';
import { Icon } from '@iconify/react';

import Box, { BoxProps } from '@mui/material/Box';

interface IconifyProps extends HTMLAttributes<HTMLDivElement> {
  icon: string;
  width?: number;
  sx?: BoxProps['sx'];
}

const Iconify = forwardRef<HTMLDivElement, IconifyProps>(
  ({ icon, width = 20, sx, ...other }, ref) => (
    <Box
      ref={ref}
      component={Icon}
      className="component-iconify"
      sx={{ width, height: width, ...sx }}
      {...other}
    />
  )
);

Iconify.displayName = 'Iconify'; 


export default Iconify;