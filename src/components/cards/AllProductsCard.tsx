"use client";

import React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import { styled } from "@mui/material/styles";
import { Avatar, Stack, Typography } from "@mui/material";
import { ThisWeekSelect } from "@/ui/this-week-select";
import { poppinsClassName } from "@/fonts/fonts";
// import { Info, InfoSubtitle, InfoTitle } from "../info-basic";

const StyledCard = styled(Card)(() => ({
  position: "relative",
  borderRadius: 16,
  padding: 15,
  backgroundColor: "#5570F1",
  boxShadow: "0 0 20px 0 rgba(0,0,0,0.12)",
  transition: "0.3s",
  "&:hover": {
    transform: "translateY(-3px)",
    boxShadow: "0 4px 20px 0 rgba(0,0,0,0.12)",
  },
}));

const ButtonLearnMore = styled(Button)(() => ({
  backgroundColor: "#fff !important",
  color: "#fb703c",
  boxShadow: "0 2px 6px #d0efef",
  borderRadius: 12,
  minWidth: 120,
  minHeight: 4,
  textTransform: "initial",
  fontSize: "0.875rem",
  fontWeight: 700,
  letterSpacing: 0,
}));

const StyledImg = styled("img")(() => ({
  position: "absolute",
  width: "40%",
  bottom: 0,
  right: 0,
  display: "block",
}));

const StyledDiv = styled("div")(() => ({
  position: "absolute",
  bottom: 0,
  right: 0,
  transform: "translate(70%, 50%)",
  borderRadius: "50%",
  backgroundColor: "red",
  padding: "40%",

  "&:before": {
    position: "absolute",
    borderRadius: "50%",
    content: '""',
    display: "block",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: "-16%",
    backgroundColor: "red",
  },
}));

const useOfferInfoStyles = () => {
  return {
    title: {
      color: "#fb703c",
      fontSize: "1.125rem",
      fontWeight: 700,
      lineHeight: 1.6,
      letterSpacing: "0.0075em",
      marginBottom: 0,
    },
    subtitle: {
      color: "#48bbb5",
      fontSize: "0.875rem",
      fontWeight: 500,
    },
  };
};

export function AllProductsCard() {
  return (
    <StyledCard>
      <Stack direction="row" justifyContent="space-between">
        <Avatar
          variant="rounded"
          sx={{
            backgroundColor: "#FFFFFF29",
            borderRadius: "8px",
          }}
        >
          <img src="/assets/icons/navbar/ic_folder2.svg" alt="Graph" />
        </Avatar>
      </Stack>

      <Box mt="46px" sx={{ display: "flex", gap: { xs: "35px", sm: "82px" } }}>
        <Stack gap="8px">
          <Typography
            color="#ffff"
            noWrap
            sx={{ fontSize: { xs: "10px", sm: "14px" } }}
          >
            All Products
          </Typography>
          <Typography
            className={poppinsClassName}
            color="#ffff"
            fontWeight="500"
            sx={{ fontSize: { xs: "10px", sm: "20px" } }}
          >
            45
          </Typography>
        </Stack>
        <Stack gap="8px">
          <Typography
            color="#ffff"
            sx={{ fontSize: { xs: "10px", sm: "14px" } }}
          >
            Active
          </Typography>
          <Typography
            className={poppinsClassName}
            color="#ffff"
            fontWeight="500"
            sx={{ fontSize: { xs: "10px", sm: "20px" } }}
            display="inline-flex"
            alignItems="center"
          >
            32
            <Typography
              ml="7px"
              sx={{ fontSize: { xs: "7px", sm: "12px" } }}
              color="#ffff"
              component="span"
            >
              +24%
            </Typography>
          </Typography>
        </Stack>
      </Box>
    </StyledCard>
  );
}
