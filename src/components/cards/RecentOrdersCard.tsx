"use client";

import React from "react";
import Card from "@mui/material/Card";
import { styled } from "@mui/material/styles";
import PropTypes from "prop-types";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { fToNow } from "@/utils/format-time";
import { interClassName } from "@/fonts/fonts";

const StyledCard = styled(Card)(() => ({
  position: "relative",
  borderRadius: 16,
  padding: 15,
  backgroundColor: "#ffff",
  boxShadow: "0 0 20px 0 rgba(0,0,0,0.12)",
  transition: "0.3s",
  "&:hover": {
    transform: "translateY(-3px)",
    boxShadow: "0 4px 20px 0 rgba(0,0,0,0.12)",
  },
}));

interface RecentOrdersCardProps {
  list: Array<{
    id: string;
    image: string;
    title: string;
    description: string;
    postedAt: string;
    status: string;
  }>;
}

export function RecentOrdersCard({ list }: RecentOrdersCardProps) {
  return (
    <StyledCard>
      <Typography
        className={interClassName}
        sx={{ fontSize: { xs: "12px", sm: "16px" }, fontWeight: "500" }}
      >
        Recent Orders
      </Typography>
      <Stack mt="23px" spacing={3}>
        {list.map((news) => (
          <NewsItem key={news.id} news={news} />
        ))}
      </Stack>
    </StyledCard>
  );
}

RecentOrdersCard.propTypes = {
  list: PropTypes.array.isRequired,
};

// ---

interface NewsItemProps {
  news: {
    id: string;
    image: string;
    title: string;
    description: string;
    postedAt: string;
    status: string;
  };
}


function NewsItem({ news }: NewsItemProps) {
  const { image, title, description, postedAt, status } = news;

  return (
    <Stack
      justifyContent="space-between"
      spacing={2}
      sx={{
        borderBottom: "1px solid #F1F3F9",
        paddingBottom: "24px",
        flexDirection: { xs: "column", sm: "row" },
        gap: "20px",
        alignItems: {xs: "flex-start", sm: "center"}
      }}
    >
      <Stack direction="row" gap="14px">
        <Box
          component="img"
          alt={title}
          src={image}
          sx={{ width: 48, height: 48, borderRadius: 1.5, flexShrink: 0 }}
        />

        <Stack justifyContent="space-between">
          <Link
            className={interClassName}
            color="#45464E"
            variant="subtitle2"
            underline="hover"
            noWrap
            sx={{ fontSize: { xs: "10px", sm: "14px" } }}
            fontWeight="400"
          >
            {title}
          </Link>

          <Typography
            className={interClassName}
            variant="body2"
            fontWeight="500"
            sx={{ color: "#33343A", fontSize: { xs: "10px", sm: "14px" } }}
            noWrap
          >
            {description}
          </Typography>
        </Stack>
      </Stack>

      <Stack justifyContent="space-between" alignItems="flex-end" gap="10px">
        <Typography
          className={interClassName}
          variant="caption"
          sx={{
            flexShrink: 0,
            color: "text.secondary",
            fontSize: { xs: "7px", sm: "12px" },
          }}
        >
          12 Sept 2022
        </Typography>

        <Typography
          className={interClassName}
          variant="caption"
          sx={{
            flexShrink: 0,
            color: "text.secondary",
            backgroundColor: status === "completed" ? "#32936F1F" : "#F57E771F",
            padding: "2px 15px",
            borderRadius: "8px",
            fontSize: { xs: "7px", sm: "12px" },
          }}
        >
          {status}
        </Typography>
      </Stack>
    </Stack>
  );
}

NewsItem.propTypes = {
  news: PropTypes.shape({
    image: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    postedAt: PropTypes.instanceOf(Date),
    status: PropTypes.string,
  }),
};
