"use client";

import React from "react";
import Card from "@mui/material/Card";
import { styled } from "@mui/material/styles";
import PropTypes from "prop-types";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { interClassName } from "@/fonts/fonts";

interface StockOrderCardProps {
  list: Array<{
    id: string;
    image: string;
    title: string;
    description: string;
    postedAt: string;
    status: string;
  }>;
}

export function StockOrderCard({ list }: StockOrderCardProps) {
  return (
    <Stack spacing={3}>
      {list.map((news) => (
        <NewsItem key={news.id} news={news} />
      ))}
    </Stack>
  );
}

StockOrderCard.propTypes = {
  list: PropTypes.array.isRequired,
};

// ----------------------------------------------------------------------

interface NewsItemProps {
  news: {
    image: string;
    title: string;
    description: string;
    postedAt?: string; // Change the type if needed
    status?: string; // Change the type if needed
  };
}

function NewsItem({ news }: NewsItemProps) {
  const { image, title, description } = news;

  return (
    <Stack
      direction="row"
      justifyContent="space-between"
      alignItems="center"
      spacing={2}
      sx={{
        width: "40%",
        paddingBottom: "17px",
        borderBottom: "1px solid #F1F3F9",
      }}
    >
      <Stack direction="row" gap="14px">
        <Box
          component="img"
          alt={title}
          src={image}
          sx={{
            width: 48,
            height: 48,
            borderRadius: 1.5,
            flexShrink: 0,
          }}
        />

        <Stack justifyContent="space-between">
          <Link
            className={interClassName}
            color="#45464E"
            variant="subtitle2"
            underline="hover"
            noWrap
            sx={{ fontSize: { xs: "10px", sm: "14px" } }}
            fontWeight="400"
          >
            {title}
          </Link>

          <Typography
            className={interClassName}
            variant="body2"
            fontWeight="500"
            sx={{ color: "#33343A", fontSize: { xs: "10px", sm: "14px" } }}
            noWrap
          >
            {description}
          </Typography>
        </Stack>
      </Stack>

      <Stack justifyContent="space-between" alignItems="flex-end" gap="10px">
        <Typography
          className={interClassName}
          variant="caption"
          sx={{ flexShrink: 0, color: "#ffff" }}
        >
          12 Sept 2022
        </Typography>

        <Typography
          className={interClassName}
          variant="caption"
          sx={{
            color: "#5570F1",
            padding: "2px 15px",
            borderRadius: "8px",
            fontSize: "12px",
            fontWeight: "500",
          }}
        >
          12{" "}
          <Typography
            className={interClassName}
            component="span"
            sx={{
              fontSize: { xs: "7px", sm: "12px" },
              fontWeight: "400",
              color: "#8B8D97",
            }}
          >
            In Stock
          </Typography>
        </Typography>
      </Stack>
    </Stack>
  );
}

NewsItem.propTypes = {
  news: PropTypes.shape({
    image: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    postedAt: PropTypes.instanceOf(Date),
    status: PropTypes.string,
  }),
};
