"use client";

import React from "react";
import Card from "@mui/material/Card";
import { styled } from "@mui/material/styles";
import { Box, Button, Divider, Link, Stack, Typography } from "@mui/material";
import { interClassName } from "@/fonts/fonts";
import { StockOrderCard } from "./StockOrderCard";
import { faker } from "@faker-js/faker";

const StyledCard = styled(Card)(() => ({
  position: "relative",
  borderRadius: 16,
  padding: "15px 0px",
  backgroundColor: "#ffff",
  boxShadow: "0 0 20px 0 rgba(0,0,0,0.12)",
  transition: "0.3s",
  "&:hover": {
    transform: "translateY(-3px)",
    boxShadow: "0 4px 20px 0 rgba(0,0,0,0.12)",
  },
}));

export function ChatCard() {
  return (
    <StyledCard>
      <Stack
        className={interClassName}
        px="15px"
        mb="15px"
        sx={{ flexDirection: { xs: "column", sm: "row" } }}
        justifyContent="space-between"
      >
        <Stack direction="row" gap="14px">
          <Box
            component="img"
            alt="user"
            src="/assets/images/profile/profilebare.png"
            sx={{ width: 48, height: 48, borderRadius: 1.5, flexShrink: 0 }}
          />

          <Stack justifyContent="space-between">
            <Link
              className={interClassName}
              color="#45464E"
              variant="subtitle2"
              underline="hover"
              noWrap
              fontSize="16px"
              fontWeight="400"
            >
              Jane Doe
            </Link>

            <Stack direction="row" gap="8px">
              <Stack direction="row" alignItems="center" gap="4px">
                <Box
                  component="img"
                  alt="dot"
                  src="/assets/icons/ic_onlinedot.svg"
                  sx={{ width: 12, height: 12 }}
                />
                <Typography fontSize="14px" color="#B6BFE8">
                  Online
                </Typography>
              </Stack>
              <Stack
                sx={{ display: { xs: "none", md: "flex" } }}
                fontSize="14px"
                color="#8B8D97"
              >
                12:55 am
              </Stack>
            </Stack>
          </Stack>
        </Stack>
        <Stack sx={{ display: { xs: "none", md: "flex" } }}>
          <Stack
            sx={{ display: { xs: "none", sm: "flex" } }}
            direction="row"
            alignItems="center"
            gap="12px"
          >
            <Typography
              className={interClassName}
              variant="caption"
              sx={{
                color: "#1C1D22",
                backgroundColor: "#FEF5EA",
                borderRadius: "8px",
                fontSize: "12px",
                padding: "0px 7px",
              }}
            >
              New Customer
            </Typography>
            <Button
              sx={{
                fontSize: "14px",
                textTransform: "capitalize",
                padding: "0px",
              }}
            >
              View Profile
            </Button>
          </Stack>
          <Stack
            direction="row"
            justifyContent="flex-end"
            alignItems="center"
            gap="8px"
          >
            <Box
              component="img"
              src="/assets/icons/navbar/ic_bag.svg"
              alt="Bag Icon"
              sx={{ width: 15.07, height: 16.02 }}
            />
            <Typography sx={{ fontSize: "14px", textTransform: "capitalize" }}>
              0 Orders
            </Typography>
          </Stack>
        </Stack>
      </Stack>

      <Divider sx={{ opacity: "0.3" }} color="#F1F3F9" />

      <Stack
        mt="21px"
        direction="row"
        className={interClassName}
        fontSize="14px"
        justifyContent="center"
      >
        <Box
          p="8px 12px"
          sx={{
            backgroundColor: "#F4F5FA",
            width: "fit-content",
            borderRadius: "8px",
          }}
        >
          12 August 2022
        </Box>
      </Stack>

      <Stack mx="15px">
        <StockOrderCard
          list={[...Array(1)].map((_, index) => ({
            id: faker.string.uuid(),
            title: "IPhone 13",
            description: "₦730,000.00 x 1",
            image: `/assets/images/covers/iphone.png`,
            postedAt: "12 Sept 2022",
            status: index % 2 === 0 ? "pending" : "completed",
          }))}
        />
      </Stack>

      <Stack mt="8px" className={interClassName} mx="15px" gap="8px">
        <Box
          sx={{
            backgroundColor: "#5570F1",
            width: { xs: "fit-content", sm: "40%" },
            padding: "16px",
            borderRadius: "16px",
            borderBottomLeftRadius: "0",
            color: "#ffff",
          }}
        >
          Hello, I want to make enquiries about your product
        </Box>
        <Typography sx={{ fontSize: "16px", color: "#8B8D97" }}>
          12:55 am
        </Typography>
      </Stack>

      <Stack
        mt="8px"
        className={interClassName}
        mx="15px"
        gap="8px"
        alignItems="flex-end"
      >
        <Box
          sx={{
            backgroundColor: "#FFEAD1",
            width: "fit-content",
            padding: "16px",
            borderRadius: "16px",
            borderBottomRightRadius: "0",
            color: "#1C1D22",
          }}
        >
          Hello Janet, thank you for reaching out
        </Box>
        <Stack direction="row" gap="7px">
          <Typography sx={{ fontSize: "16px", color: "#8B8D97" }}>
            12:55 am
          </Typography>
          <Box
            component="img"
            src="/assets/icons/ic_check_bg.svg"
            alt="check"
          />
        </Stack>
      </Stack>

      <Stack
        mt="20px"
        className={interClassName}
        mx="15px"
        gap="8px"
        alignItems="flex-end"
      >
        <Box
          sx={{
            backgroundColor: "#FFEAD1",
            width: "fit-content",
            padding: "16px",
            borderRadius: "16px",
            borderBottomRightRadius: "0",
            color: "#1C1D22",
          }}
        >
          What do you need to know?
        </Box>
        <Stack direction="row" gap="7px">
          <Typography sx={{ fontSize: "16px", color: "#8B8D97" }}>
            12:55 am
          </Typography>
          <Box
            component="img"
            src="/assets/icons/ic_check_bg.svg"
            alt="check"
          />
        </Stack>
      </Stack>

      <Stack
        mt="21px"
        direction="row"
        className={interClassName}
        fontSize="14px"
        justifyContent="center"
      >
        <Box
          p="8px 12px"
          sx={{
            backgroundColor: "#F4F5FA",
            width: "fit-content",
            borderRadius: "8px",
          }}
        >
          Today
        </Box>
      </Stack>

      <Stack mt="20px" className={interClassName} mx="15px" gap="8px">
        <Box
          sx={{
            backgroundColor: "#5570F1",
            width: { xs: "fit-content", sm: "40%" },
            padding: "16px",
            borderRadius: "16px",
            borderBottomLeftRadius: "0",
            color: "#ffff",
          }}
        >
          Hello, I want to make enquiries about your product
        </Box>
        <Typography sx={{ fontSize: "16px", color: "#8B8D97" }}>
          12:55 am
        </Typography>
      </Stack>
    </StyledCard>
  );
}
