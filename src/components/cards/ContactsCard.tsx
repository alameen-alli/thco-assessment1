import React from "react";
import Card from "@mui/material/Card";
import { styled } from "@mui/material/styles";
import PropTypes from "prop-types";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import SearchIcon from "@mui/icons-material/Search";
import { poppinsClassName } from "@/fonts/fonts";
import {
  FormControl,
  IconButton,
  InputAdornment,
  outlinedInputClasses,
  selectClasses,
  TextField,
} from "@mui/material";
import MessageItems from "@/ui/message-item";

const StyledCard = styled(Card)(() => ({
  position: "relative",
  borderRadius: 16,
  // padding: 15,
  backgroundColor: "#ffff",
  minWidth: 300,
  boxShadow: "0 0 20px 0 rgba(0,0,0,0.12)",
  transition: "0.3s",
  "&:hover": {
    transform: "translateY(-3px)",
    boxShadow: "0 4px 20px 0 rgba(0,0,0,0.12)",
  },
}));

interface ContactsCardProps {
  list: any[]; // Replace 'any' with the appropriate type for the 'list' prop
}

export function ContactsCard({ list }: ContactsCardProps) {
  return (
    <StyledCard>
      <Stack
        sx={{ padding: "24px 24px 0px" }}
        direction="row"
        justifyContent="space-between"
      >
        <Typography
          className={poppinsClassName}
          fontSize="20px"
          fontWeight="500"
          color="#2C2D33"
        >
          Contacts
        </Typography>
        <Typography fontSize="20px" fontWeight="500" color="#A6A8B1">
          34
        </Typography>
      </Stack>

      <FormControl
        sx={{ maxWidth: "560.5px", width: "100%", padding: "0px 24px" }}
      >
        <TextField
          variant="outlined"
          fullWidth
          placeholder="Search..."
          InputProps={{
            startAdornment: (
              <InputAdornment position="end">
                <IconButton aria-label="search">
                  <SearchIcon />
                </IconButton>
              </InputAdornment>
            ),
          }}
          sx={{
            borderRadius: "8px",
            mt: "15px",
            [`& .${selectClasses.select}`]: {
              background: "#F2FAFE1F",
              color: "#A2A6B0",
              borderRadius: "8px",
              paddingLeft: "12px",
              paddingTop: "8px",
              paddingBottom: "8px",
            },
            [`& .${outlinedInputClasses.notchedOutline}`]: {
              borderColor: "grey.300",
              borderRadius: "12px",
              borderStyle: "solid",
              borderWidth: "2px",
            },
            "&:hover": {
              [`& .${outlinedInputClasses.notchedOutline}`]: {
                borderColor: "grey.400",
              },
            },
          }}
        />
      </FormControl>

      <Stack mt="25px" spacing={3}>
        <MessageItems />
      </Stack>
    </StyledCard>
  );
}

ContactsCard.propTypes = {
  list: PropTypes.array.isRequired,
};

export default ContactsCard;
