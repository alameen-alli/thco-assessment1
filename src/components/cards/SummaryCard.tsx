"use client";

import React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import { styled } from "@mui/material/styles";
import { Stack, Typography } from "@mui/material";
import { ThisWeekSelect } from "@/ui/this-week-select";
import PieClick from "@/ui/pie-chart";
import { SalesCard } from "./SalesCard";
import { SalesSelect } from "@/ui/sales-select";
import { DaySelect } from "@/ui/day-select";
import BarClick from "@/ui/bar-chart";

const StyledCard = styled(Card)(() => ({
  height: "512px",
  position: "relative",
  borderRadius: 16,
  padding: 15,
  backgroundColor: "#ffff",
  boxShadow: "0 0 20px 0 rgba(0,0,0,0.12)",
  transition: "0.3s",
  "&:hover": {
    transform: "translateY(-3px)",
    boxShadow: "0 4px 20px 0 rgba(0,0,0,0.12)",
  },
}));

export function SummaryCard() {
  return (
    <StyledCard>
      <Stack height="100%" justifyContent="space-between">
        <Stack
          sx={{ flexDirection: { xs: "column", sm: "row" } }}
          justifyContent="space-between"
        >
          <Stack
            gap="21px"
            sx={{ flexDirection: { xs: "column", sm: "row" } }}
            alignItems="center"
          >
            <Typography fontWeight="500" fontSize="16px">
              Summary
            </Typography>
            <SalesSelect />
          </Stack>
          <DaySelect />
        </Stack>

        <Box>
          <BarClick />
        </Box>
      </Stack>
    </StyledCard>
  );
}
