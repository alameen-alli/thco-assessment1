"use client";

import React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import { styled } from "@mui/material/styles";
import { Avatar, Stack, Typography } from "@mui/material";
import { ThisWeekSelect } from "@/ui/this-week-select";
import { poppinsClassName } from "@/fonts/fonts";
import PieClick from "@/ui/pie-chart";
// import { Info, InfoSubtitle, InfoTitle } from "../info-basic";

const StyledCard = styled(Card)(() => ({
  height: "375px",
  position: "relative",
  borderRadius: 16,
  padding: 15,
  backgroundColor: "#ffff",
  boxShadow: "0 0 20px 0 rgba(0,0,0,0.12)",
  transition: "0.3s",
  "&:hover": {
    transform: "translateY(-3px)",
    boxShadow: "0 4px 20px 0 rgba(0,0,0,0.12)",
  },
}));

const ButtonLearnMore = styled(Button)(() => ({
  backgroundColor: "#fff !important",
  color: "#fb703c",
  boxShadow: "0 2px 6px #d0efef",
  borderRadius: 12,
  minWidth: 120,
  minHeight: 4,
  textTransform: "initial",
  fontSize: "0.875rem",
  fontWeight: 700,
  letterSpacing: 0,
}));

const StyledImg = styled("img")(() => ({
  position: "absolute",
  width: "40%",
  bottom: 0,
  right: 0,
  display: "block",
}));

const StyledDiv = styled("div")(() => ({
  position: "absolute",
  bottom: 0,
  right: 0,
  transform: "translate(70%, 50%)",
  borderRadius: "50%",
  backgroundColor: "red",
  padding: "40%",

  "&:before": {
    position: "absolute",
    borderRadius: "50%",
    content: '""',
    display: "block",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: "-16%",
    backgroundColor: "red",
  },
}));

const useOfferInfoStyles = () => {
  return {
    title: {
      color: "#fb703c",
      fontSize: "1.125rem",
      fontWeight: 700,
      lineHeight: 1.6,
      letterSpacing: "0.0075em",
      marginBottom: 0,
    },
    subtitle: {
      color: "#48bbb5",
      fontSize: "0.875rem",
      fontWeight: 500,
    },
  };
};

export function MarketingCard() {
  return (
    <StyledCard>
      <Stack direction="row" justifyContent="space-between" alignItems="center">
        <Typography
          fontWeight="500"
          sx={{ fontSize: { xs: "14px", sm: "16px" } }}
        >
          Marketing
        </Typography>
        <ThisWeekSelect />
      </Stack>

      <Box mt="12px" sx={{ display: "flex", justifyContent: "space-between" }}>
        <Stack direction="row" alignItems="center" gap="8px">
          <img
            alt="rounded"
            width={8}
            height={8}
            src="/assets/icons/navbar/ic_bluebullet.svg"
          />
          <Typography
            color="#A6A8B1"
            sx={{ fontSize: { xs: "8px", sm: "11px" } }}
          >
            Acquisition
          </Typography>
        </Stack>

        <Stack direction="row" alignItems="center" gap="8px">
          <img
            alt="rounded"
            width={8}
            height={8}
            src="/assets/icons/navbar/ic_purplebullet.svg"
          />
          <Typography
            color="#A6A8B1"
            sx={{ fontSize: { xs: "8px", sm: "11px" } }}
          >
            Purchase
          </Typography>
        </Stack>

        <Stack direction="row" alignItems="center" gap="8px">
          <img
            alt="rounded"
            width={8}
            height={8}
            src="/assets/icons/navbar/ic_yellowbullet.svg"
          />
          <Typography
            color="#A6A8B1"
            sx={{ fontSize: { xs: "8px", sm: "11px" } }}
          >
            Retention
          </Typography>
        </Stack>
      </Box>

      <PieClick />
    </StyledCard>
  );
}
