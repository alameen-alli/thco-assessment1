import SvgColor from './svg-color/svg-color';

// Define a type for the navigation item
interface NavItem {
  title: string;
  path: string;
  icon: JSX.Element; // Assuming that the icon is a JSX element
}

// Define the icon function
const icon = (name: string): JSX.Element => (
  <SvgColor src={`/assets/icons/navbar/${name}.svg`} sx={{ width: 1, height: 1 }} />
);

// Define the navigation configuration array
const navConfig: NavItem[] = [
  {
    title: 'dashboard',
    path: '/',
    icon: icon('ic_dashboard'),
  },
  {
    title: 'orders',
    path: '/orders',
    icon: icon('ic_bag'),
  },
  {
    title: 'customers',
    path: '/customers',
    icon: icon('ic_users'),
  },
  {
    title: 'inventory',
    path: '/inventory',
    icon: icon('ic_folder'),
  },
  {
    title: 'conversations',
    path: '/conversations',
    icon: icon('ic_chat'),
  },
  {
    title: 'settings',
    path: '/settings',
    icon: icon('ic_settings'),
  },
];

export default navConfig;
