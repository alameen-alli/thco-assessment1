import { forwardRef, HTMLAttributes } from 'react';
import PropTypes from 'prop-types';

import Box, { BoxProps } from '@mui/material/Box';

interface SvgColorProps extends HTMLAttributes<HTMLSpanElement> {
  src: string;
  sx?: BoxProps['sx'];
}

const SvgColor = forwardRef<HTMLSpanElement, SvgColorProps>(
  ({ src, sx, ...other }, ref) => (
    <Box
      component="span"
      className="svg-color"
      ref={ref}
      sx={{
        width: 24,
        height: 24,
        display: 'inline-block',
        bgcolor: 'currentColor',
        mask: `url(${src}) no-repeat center / contain`,
        WebkitMask: `url(${src}) no-repeat center / contain`,
        ...sx,
      }}
      {...other}
    />
  )
);

SvgColor.displayName = 'SvgColor'; // Specify the display name here


SvgColor.propTypes = {
  src: PropTypes.string.isRequired,
  sx: PropTypes.object,
};

export default SvgColor;
