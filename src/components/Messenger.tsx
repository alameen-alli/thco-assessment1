import * as React from "react";
import { Grid } from "@mui/material";
import { ContactsCard } from "./cards/ContactsCard";
import { faker } from "@faker-js/faker";
import { ChatCard } from "./cards/ChatCard";

export default function Messenger() {
  return (
    <Grid mt="25px" spacing={3} container>
      <Grid item sx={{ display: { xs: "none", lg: "block" } }} lg={4}>
        <ContactsCard
          list={[...Array(9)].map((_, index) => ({
            id: faker.string.uuid(),
            title: "IPhone 13",
            description: "₦730,000.00 x 1",
            image: `/assets/images/covers/iphone.png`,
            postedAt: "12 Sept 2022",
            status: index % 2 === 0 ? "pending" : "completed",
          }))}
        />
      </Grid>
      <Grid item xs={12} lg={8}>
        <ChatCard />
      </Grid>
    </Grid>
  );
}
