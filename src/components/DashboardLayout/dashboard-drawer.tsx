"use client";

import {
  Box,
  Button,
  IconButton,
  ListItemButton,
  styled,
  Theme,
  Toolbar,
  Typography,
  useMediaQuery,
} from "@mui/material";
import MuiDrawer from "@mui/material/Drawer";
import React, { useEffect } from "react";
import useDrawerStore from "@/store/useDrawerStore";
import NavigationList from "./nav-list";
import { interClassName, poppinsClassName } from "@/fonts/fonts";
import { ChevronLeftRounded, ChevronRightRounded } from "@mui/icons-material";

const drawerWidth: number = 296;

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  "& .MuiDrawer-paper": {
    position: "relative",
    whiteSpace: "nowrap",
    borderWidth: "0px",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    boxSizing: "border-box",
    ...(!open && {
      overflowX: "hidden",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      width: "88px",
      [theme.breakpoints.up("sm")]: {
        width: "88px",
      },
    }),
  },
}));

const DashboardDrawer = () => {
  const { open, toggleDrawer } = useDrawerStore();
  const isMobile = useMediaQuery<Theme>((theme) => theme.breakpoints.down("sm")); // Provide type annotation for theme

  // Close the drawer when the screen size is below "sm"
  useEffect(() => {
    if (isMobile) {
      toggleDrawer(); // Set open state to false when screen size is below "sm"
    }
  }, [isMobile, toggleDrawer]);

  return (
    <Drawer
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        height: "100vh",
      }}
      variant="permanent"
      open={open}
    >
      <Box>
        <Toolbar
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            px: { xs: "18px" },
            py: "14px",
          }}
        >
          <Box sx={{ display: "flex", gap: "5.5px", alignItems: "center" }}>
            <img src="/logo.png" alt="Logo" />
            {open && (
              <Typography
                className={poppinsClassName}
                sx={{ display: { xs: "none", sm: "block" } }}
                fontSize="20px"
                fontWeight="700"
                color="#45464E"
              >
                Metrix
              </Typography>
            )}
          </Box>

          <IconButton onClick={toggleDrawer}>
            <ChevronLeftRounded sx={{ color: "#5570F1" }} />
          </IconButton>
        </Toolbar>

        <NavigationList />
      </Box>

      <Box sx={{ marginTop: "auto", px: 2 }}>
        <ListItemButton
          href={"#"}
          sx={{
            borderRadius: "10px",
            typography: "body2",
            display: "flex",
            gap: "18px",
            color: "#53545C",
            textTransform: "capitalize",
            fontWeight: "fontWeightMedium",
            backgroundColor: "#5E63661A",
          }}
        >
          <Box
            component="span"
            className={interClassName}
            sx={{
              width: 24,
              height: 24,
            }}
          >
            <img alt="" src="/assets/icons/navbar/ic_headphone.svg" />
          </Box>
          {open && (
            <Box component="span" fontSize="14px">
              Contact Support
            </Box>
          )}
        </ListItemButton>

        <ListItemButton
          href={"#"}
          sx={{
            // padding: "16px 20px",
            borderRadius: "10px",
            typography: "body2",
            color: "#53545C",
            textTransform: "capitalize",
            fontWeight: "fontWeightMedium",
            backgroundColor: "#FFCC9133",
            display: "flex",
            gap: "16px",
            flexDirection: "column",
            marginTop: "14px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              gap: "18px",
              width: "100%",
            }}
          >
            <Box
              component="span"
              className={interClassName}
              sx={{
                width: 24,
                height: 24,
              }}
            >
              <img alt="" src="/assets/icons/navbar/ic_gift.svg" />
            </Box>
            {open && (
              <Box component="span" fontSize="14px">
                Free Gift Awaits You!
              </Box>
            )}
          </Box>

          {open && (
            <Button
              color={"primary"}
              fullWidth
              sx={{
                textTransform: "initial",
                justifyContent: "flex-start",
                color: "#6E7079",
              }}
            >
              Upgrade your account <ChevronRightRounded />
            </Button>
          )}
        </ListItemButton>

        <Button
          color={"primary"}
          fullWidth
          sx={{
            textTransform: "initial",
            justifyContent: "flex-start",
            color: "#CC5F5F",
            fontSize: "14px",
            marginTop: "23px",
            py: "11px",
            pl: "11px",
          }}
          startIcon={<img alt="" src="/assets/icons/navbar/ic_logout.svg" />}
        >
          {open && "Logout"}
        </Button>
      </Box>
    </Drawer>
  );
};

export default DashboardDrawer;
