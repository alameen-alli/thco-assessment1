"use client";

import {
  Badge,
  Box,
  Breadcrumbs,
  Divider,
  IconButton,
  Stack,
  styled,
  Toolbar,
  Typography,
} from "@mui/material";
import MuiAppBar, { AppBarProps as MuiAppBarProps } from "@mui/material/AppBar";
import MenuIcon from "@mui/icons-material/Menu";
import useDrawerStore from "@/store/useDrawerStore";
import AccountPopover from "@/ui/account-popover";
// import NotificationsPopover from "@/ui/notifications-popover";
import { SelectBar } from "@/ui/select-bar";
import { poppinsClassName } from "@/fonts/fonts";
import IconBreadcrumbs from "@/ui/bread-crumbs";

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const drawerWidth: number = 296;

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})<AppBarProps>(({ theme, open }) => ({
  backgroundColor: "#ffff",
  zIndex: theme.zIndex.drawer,
  height: "94px",
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DashboardAppBar = ({ pathname }: { pathname: string }) => {
  const { open, toggleDrawer } = useDrawerStore();

  // Extract the text without the leading slash
  const pageTitle = pathname === "/" ? "Dashboard" : pathname.substring(1);

  return (
    <AppBar position="absolute" elevation={0} open={open}>
      <Toolbar
        sx={{
          pr: "24px",
        }}
      >
        <IconButton
          edge="start"
          color="inherit"
          aria-label="open drawer"
          onClick={toggleDrawer}
          sx={{
            marginRight: "36px",
            marginLeft: "88px",
            ...(open && { display: "none" }),
          }}
        >
          <MenuIcon sx={{ color: "#5570F1" }} />
        </IconButton>

        <Typography
          color="#45464E"
          fontWeight={500}
          className={poppinsClassName}
          noWrap
          sx={{ flexGrow: 1, fontSize: "20px", textTransform: "capitalize" }}
        >
          {pageTitle}
        </Typography>

        <Stack direction="row" alignItems="center" spacing={3}>
          <SelectBar />
          {/* <NotificationsPopover /> */}
          <AccountPopover />
        </Stack>
      </Toolbar>

      <Divider sx={{ opacity: "0.3" }} color="#F1F3F9" />

      <Box sx={{ ml: open ? "25px" : "120px", my: "4px" }}>
        <IconBreadcrumbs />
      </Box>
    </AppBar>
  );
};

export default DashboardAppBar;
