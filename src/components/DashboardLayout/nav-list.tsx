import { alpha, ListItemButton, Stack } from "@mui/material";
import navConfig from "../config-navigation";
import { usePathname } from "next/navigation";
import PropTypes from "prop-types";
import React from "react";
import Box from "@mui/material/Box";
import { interClassName } from "@/fonts/fonts";
import useDrawerStore from "@/store/useDrawerStore";

interface NavItemProps {
  item: {
    title: string;
    path: string;
    icon: JSX.Element;
  };
}

function NavItem({ item }: NavItemProps) {
  const { open } = useDrawerStore(); // Destructure the open state from the hook
  const pathname = usePathname();
  const active = item.path === pathname;


  return (
    <ListItemButton
      href={item.path}
      sx={{
        padding: "16px 20px",
        borderRadius: "10px",
        typography: "body2",
        display: "flex",
        gap: '18px',
        color: "#53545C",
        textTransform: "capitalize",
        fontWeight: "fontWeightMedium",
        ...(active && {
          color: "#ffff",
          fontWeight: "fontWeightSemiBold",
          bgcolor: "#5570F1",
          "&:hover": {
            bgcolor: (theme) => alpha(theme.palette.primary.main, 0.16),
          },
        }),
      }}
    >
      <Box
        component="span"
        className={interClassName}
        sx={{
          width: 24,
          height: 24,
          ...(active && {
            color: "#ffff",
            fontWeight: "fontWeightSemiBold",
          }),
        }}
      >
        {item.icon}
      </Box>
      {open && (
        <Box component="span" fontSize="14px">
          {item.title}
        </Box>
      )}
    </ListItemButton>
  );
}

NavItem.propTypes = {
  item: PropTypes.object.isRequired,
};

const NavigationList = () => {
  const renderMenu = (
    <Stack mt="62px" component="nav" spacing={0.5} sx={{ px: 2 }}>
      {navConfig.map((item) => (
        <NavItem key={item.title} item={item} />
      ))}
    </Stack>
  );

  return renderMenu;
};

export default NavigationList;
