// useDrawerStore.ts
import { create } from "zustand";

interface DrawerState {
  open: boolean;
  toggleDrawer: () => void;
}

const useDrawerStore = create<DrawerState>((set) => ({
  open: true,
  toggleDrawer: () => set((state) => ({ open: !state.open })),
}));

export default useDrawerStore;
